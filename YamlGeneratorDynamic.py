from logging.handlers import RotatingFileHandler
import re
import cv2
import numpy as np
import os
import sys
import time
import configparser
import yaml

import utils

config = configparser.ConfigParser()
config.read('yamlGenerator.ini')

blackVal=0
grayVal = 205
whiteVal = 254
displayPick = 1
displayScale = 0.15
savefilename = "testSave.png"
filename = "testImage_2"
readfilename = filename + ".png"

configProfile = 'DEFAULT'



resolution = float(config[configProfile]['resolution'])

# Make sure negate is integer (0 or 1). Will be treated as a boolean so a float value like
# 0.0 will confuse whatever ROS packages you're using
negate = int(config[configProfile]['negate'])

occupied_thresh =  float(config[configProfile]['occupied_thresh'])
free_thresh = float(config[configProfile]['free_thresh'])

blackVal = int(config[configProfile]['blackVal'])
grayVal = int(config[configProfile]['grayVal'])
whiteVal = int(config[configProfile]['whiteVal'])

# Init Filename here:
filename = config[configProfile]['filename']

# Init wether to chooe display pick
displayPick = int(config[configProfile]['displayPick'])
displayScale = float(config[configProfile]['displayScale'])

# Option of whether to read from Ycoord or Xcoord:
coordOpt = int(config[configProfile]['coordOpt'])

# Init origin using Pixel Vals:
inYcoord = int(config[configProfile]['Ycoord'])
inXcoord = int(config[configProfile]['Xcoord'])

# Init origin using Yaml Vals
xYaml = float(config[configProfile]['xYaml'])
yYaml = float(config[configProfile]['yYaml'])
yawYaml = float(config[configProfile]['yawYaml'])

#INIT ROTATE ANGLE AND TRANSLATION HERE:
rotation_angle = float(config[configProfile]['rotation_angle'])

# Option to remove gray blur:
removeBlur = int(config[configProfile]['removeBlur'])

yTranslate = int(config[configProfile]['yTranslate'])
xTranslate = int(config[configProfile]['xTranslate'])

def empty(a):
    pass

if __name__=='__main__':
    # Read commandline args:
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        # Moved these to config ini file-delete this later
    
    if filename.endswith(".png"):
        # print("filename ended with png")
        readfilename = filename
        filename = filename[:-(len(".png"))]
    else:
        # print("filename needs png")
        readfilename = filename + ".png"
        
    # Values to help print later:
    rotation_string = str(rotation_angle)
    print("filename: ", filename)
    print("readfilename: ", readfilename)
    orig_filename = filename

    height = 0
    width = 0
    try:
        img = cv2.imread(readfilename, 0)
        height=0
        width=0
        try:
            height, width= img.shape
        except:
            height, width, something= img.shape
        print("Image dimension:", height, width)
        print("original uniques: ", np.unique(img))
    except Exception as e:
        print(e)
        print("Error!: Ensure that you are inputting valid filename without png exstension")

    # Initialize Ycoord and Xcoord
    Ycoord = 0
    Xcoord = 0

    # If we wish to use manually supplied values
    if displayPick==0:
        print("displayPick=0")
        print("READING FROM INI SPECIFIED VALUES")

        if coordOpt==0:
            print("READING FROM PROVIDED YAML COORDS: ")
            print("yYaml: ", yYaml)
            print("xYaml: ", xYaml)
            print("yawYaml: ", yawYaml)
            # def generatePixelOrigin(height, width, yYaml, xYaml, resolution = 0.050000):
            retPXCoords = utils.generatePixelOrigin(height, width, yYaml, xYaml, resolution)
            Ycoord = retPXCoords[0]
            Xcoord = retPXCoords[1]

            print("Pixel coords from YAML COORDS:")
            print("Ycoord: ", Ycoord)
            print("Xcoord: ", Xcoord)

        #Usig provided Ycoord and Xcoord from ini file
        elif coordOpt==1:
            Ycoord = inYcoord
            Xcoord = inXcoord
            print("Using ini-Specified Pixel coords:")
            print("Ycoord: ", Ycoord)
            print("Xcoord: ", Xcoord)

    # If we want to manually pick a point visually
    elif displayPick==1:
        print("displayPick=1")
        print("Opening up UI to manually pick coord:")
        returnPoints= utils.displayPickOrigin(img, displayScale=displayScale)
        # returnPoints is (y, x)
        Ycoord = returnPoints[0]
        Xcoord = returnPoints[1]

        # displayIMG = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
        # cv2.circle(displayIMG, (Xcoord, Ycoord), 30, (0,255,0), cv2.FILLED)

    else:
        print("ERROR INVALID ARG:")
        print("displayPick=", displayPick)
        exit()
    
    # We now have height, width, Ycoord, and Xcoord
    yamlOriginCoords = utils.generateYamlVals(height, width, Ycoord, Xcoord, resolution)
    print("NON-ROTATED [x, y, orientation]: \n",yamlOriginCoords)
    print()


    # Do translation stuff here before complicating it with rotation
    # Using np.roll(<img>, shift_val, axis = <axis>)
    # ^for axis: y-axis=0 and x-axis=1 (depends on what depth in the numpy array kinda)
    ###############################################################################################################################3
    print("SHIFTING BY: ")
    print("Y: ", yTranslate)
    print("X: ", xTranslate)
    translateSTR = "_Transl-"+ str(yTranslate)+"_"+str(xTranslate)

    # In case filename is inside a path:
    filename = os.path.basename(filename)

    # ROLL THE IMAGE
    filename = filename + translateSTR
    img = np.roll(img, yTranslate, axis=0)
    img = np.roll(img, xTranslate, axis=1)

    # Apply the coord math to the pixel coords
    Ycoord = (Ycoord+yTranslate)
    Xcoord = (Xcoord+xTranslate)

    if Ycoord>=height:
        Ycoord = Ycoord%height
    if Xcoord>=height:
        Xcoord = Xcoord%width

    # Ensure that both are positive vals
    if Ycoord<0:
        Ycoord = Ycoord+height
    if Xcoord<0:
        Xcoord = Xcoord+width

    print("New Translated Origin:" )
    print("Y: ", Ycoord)
    print("X: ", Xcoord)


    # NOTE:
    # We will split img to one with grayVal and one with noGrayVal.
    # Reason, when having the noGrayVal, can attempt to run filters
    # for concentration, eliminating grayscale blur from rotation, etc
    # Gray only image-test black
    onlyGrayimg = np.where(img!=grayVal, whiteVal, img)
    # 0 for black and 254 for white
    noGrayimg = np.where(img==grayVal, whiteVal, img)

    # Redudundant code BUT left here in case want to reschale image above and the dimensions change
    try:
        h, w , s = img.shape
    except:
        h, w = img.shape

    # NOTE:
    # Numpy coords use (height, width)
    # BUT CV2 uses (width, height)
    # ^same axis direction and values just swapped

    center = (w//2, h//2)
    yamlRotatedOriginCoords, PXRotatedCoords = utils.generateRotatedYamlVals(h, w, Ycoord, Xcoord, rotation_angle, resolution)
    print("ROTATED BY ", rotation_angle, " degrees new YAML COORDS: ", yamlRotatedOriginCoords)


    # args(<center>, angle, scale)
    # positive angle for counterclockwise and negative for clockwise
    # Here we are applying a (-1) to the angle to make the rotatoin "clockwise"
    rotation_matrix = cv2.getRotationMatrix2D(center, (-1*rotation_angle), 1.0)

    # get noGray and onlyGray rotated for later use
    noGrayRotated = cv2.warpAffine(noGrayimg, rotation_matrix, (w,h), borderValue=grayVal)
    onlyGrayRotated = cv2.warpAffine(onlyGrayimg, rotation_matrix, (w,h), borderValue=grayVal)

    # HERE: we can run filters on the noGrayRotated to attempt to eliminate grayscale blur
    #Make values extreme
    # remember, np.where = (<ifstatement>, TrueVal, FalseVal)


    if removeBlur==1:
        # threshold=(whiteVal/2)
        # noGrayRotated= np.where(noGrayRotated<threshold, blackVal, noGrayRotated )
        # noGrayRotated=np.where(noGrayRotated>=threshold, blackVal, noGrayRotated )

        # eliminate blur for non-gray
        noGrayRotated=np.where(noGrayRotated<whiteVal, blackVal, whiteVal)
        # enforce uint8 as python integer probably has too many bits
        noGrayRotated = noGrayRotated.astype("uint8")

        # eliminate blur for onlygray
        onlyGrayRotated = np.where(onlyGrayRotated<whiteVal, grayVal, whiteVal)
        # enforce uint8 as python integer probably has too many bits
        onlyGrayRotated = onlyGrayRotated.astype("uint8")



        # onlyGrayRotated = np.where(onlyGrayRotated!=whiteVal, grayVal,  onlyGrayRotated)



    # Combine rotates of nogray and only gray
    final_combined_rotated = np.where(onlyGrayRotated!=whiteVal, onlyGrayRotated, noGrayRotated)




    # create the directory
    dirname = orig_filename + "_processed"
    # path = './'
    # path = path + dirname
    # isExist = os.path.exists(path)
    isExist = os.path.exists(dirname)

    if not isExist:
        os.mkdir(dirname)
    else:
        print("NOTE: DIRECTORY ", dirname ," ALREADY EXISTS")
    
    print
    print("SAVING TO DIR: ", dirname)
    print("......")



    cv2.imwrite(os.path.join(dirname,filename + "_ROTATED_" + rotation_string + ".png"), final_combined_rotated)
    print("Saved Image filename: ", filename + "_ROTATED_" + rotation_string + ".png")

    
    # Create display Image
    displayIMG = cv2.cvtColor(final_combined_rotated, cv2.COLOR_GRAY2RGB)
    cv2.circle(displayIMG, (PXRotatedCoords[1], PXRotatedCoords[0]), 30, (0,255,0), cv2.FILLED)
    cv2.imwrite(os.path.join(dirname,filename + "_ROTATED_" + rotation_string + "_DISPLAY-START_" + ".png"), displayIMG)
    print("Saved Display Start Image: ", filename +"_ROTATED_" + rotation_string + "_DISPLAY-START_" + ".png")
   

    data = {
        'image':(filename+"_ROTATED_"+rotation_string + ".png"),
        'resolution':resolution,
        'origin': [float(f) for f in yamlRotatedOriginCoords],
        'negate':negate,
        'occupied_thresh':occupied_thresh,
        'free_thresh':free_thresh

    }

    f = open(os.path.join(dirname, filename+"_ROTATED_"+rotation_string + ".yaml"), "w")

    # FOR Python 3:
    # yaml.dump(data, f, default_flow_style=None, sort_keys=False)
    # For Python 2:
    yaml.dump(data, f, default_flow_style=None)
    
    print("Saved Yaml filename: ", filename+"_ROTATED_"+rotation_string + ".yaml")
    f.close()

    print("DONE!")