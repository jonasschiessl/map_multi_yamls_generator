import cv2
import numpy as np

import sys

import utils

savefilename = "testSave.png"
filename = "testImage_2"
readfilename = filename + ".png"
# cv2.namedWindow("Concentration")
# cv2.resizeWindow("Concentration", 640, 480)

def empty(a):
    pass

if __name__=='__main__':
    if len(sys.argv) > 1:
        filename = sys.argv[1]

        if filename.endswith(".png"):
            readfilename = filename
            filename = filename[:-(len(".png"))]
        else:
            readfilename = filename + ".png"
    
        xYaml = float(sys.argv[2])
        yYaml = float(sys.argv[3])
        print("YAML:")
        print("xYaml: ", xYaml)
        print("YYaml: ", yYaml)


    else:
        print("ERROR")
        # exit()

    print("filename: ", filename)
    print("readfilename: ", readfilename)

    height = 0
    width = 0

    try:
        img = cv2.imread(readfilename, cv2.IMREAD_UNCHANGED)
        # print("IMG SHAPE: ", img.shape)
        height=0
        width=0
        try:
            height, width= img.shape
        except:
            height, width, something= img.shape
        print("Image dimension:", height, width)
        # print("original uniques: ", np.unique(img))
    except Exception as e:
        print(e)
        print("Error!: Ensure that you are inputting valid filename without png exstension")


    # We now have height, width, yYaml, and xYaml

    originPX = utils.generatePixelOrigin(height, width, yYaml, xYaml)
    print("PX origin from yaml:")
    print(originPX)
    print("yPxCoord: ", originPX[0])
    print("xPxCoord: ", originPX[1])