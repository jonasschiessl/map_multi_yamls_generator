YAML Generator Tool:

-Developer NOTE: 
-Python3 and Python2 yaml dump args are diff
    -Python2 sort_keys=False is not a recognized argument
    -There should be a commented out sections at bottom of YamlGeneratorDynamic.py to change if needed


USAGE:
    Method 1- DISPLAY PICK:
        -for this option, ensure displayPick in yamlGenerator.ini is set to 1
        STEP 1:
        Open yamlGenerator.ini and ensure following values are correct:

            -Yaml config:
                resolution, negate, occupied_thresh, and free_thresh
                -most likely don't change these
            -filename:
                -Ignore for this option for now

            -Display pick options
                -displayPick: set to 1
                -displayScale: Image is likely big, so use this value to scale down image for convenient display and clicking

            -coord Vals:
                -Ignore these for now. Since we chose displayPick=1, we will be picking a point by visual and hand

            -Rotation:
                -rotation_angle: set angle to whatever angle you wish to rotate the image by
            -Translation:
                -yTranslate: translate image up or down by however many pixels
                -xTranslate: translate image left or right by however many pixels
    
            -ColorVals:
                -ignore for now
        
        STEP 2:
            -run command:
            ">python YamlGeneratorDynamic <path to image>"
        STEP 3:
            -a display of the image you wish to generate a yaml for should appear, simply click on the image where you wish the starting origi
                to be set and wait for the window to close and the script to finish
        STEP 4:
            -a folder called <image name>_processed should be created in the same directory as your image with the image, yaml file, and display image showing a green
                dot where the origin was selected
_______________________________________________________________________________________________________________________
    Method 2- MANUAL COORDS:
        -for this option, ensure displayPick in yamlGenerator.ini is set to 0
        -To choose between entering yaml origin coords and pixel coords, set the param: coordOpt to 0 or 1
        STEP 1:
        Open yamlGenerator.ini and ensure following values are correct:

            -Yaml config:
                resolution, negate, occupied_thresh, and free_thresh
                -most likely don't change these
            -filename:
                -Ignore for this option for now

            -Display pick options
                -displayPick: set to 0
                -displayScale: Image is likely big, so use this value to scale down image for convenient display and clicking
                    -will not matter however as we are NOT using the displayPick option

            -coordOpt:
                -coordOpt=0:
                    -will read origin points from Yaml coords:(yYaml, xYaml, and yawYaml)
                -coordOpt=1:
                    -will read origin points from image Pixel coords:(Ycoord, and Xcoord)

            -coord Vals:
                -Yaml coords (coordOpt=0):
                    -set yYaml, xYaml, and yawYaml to corresponding yaml coords of an origin

                -Pixel coords (coordOpt=1):
                    -set Ycoord and Xcoord to desired coordinates of our origin on the image
                

            -Rotation:
                -rotation_angle: set angle to whatever angle you wish to rotate the image by
            -Translation:
                -yTranslate: translate image up or down by however many pixels
                -xTranslate: translate image left or right by however many pixels
    
            -ColorVals:
                -ignore for now
        
        STEP 2:
            -run command:
            ">python YamlGeneratorDynamic <path to image>"
        STEP 3:
            -a display of the image you wish to generate a yaml for should appear, simply click on the image where you wish the starting origi
                to be set and wait for the window to close and the script to finish
        STEP 4:
            -a folder called <image name>_processed should be created in the same directory as your image with the image, yaml file, and display image showing a green
                dot where the origin was selected


