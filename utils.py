import cv2
import numpy as np
import os
import time
import math
import yaml



blackVal = 0
grayVal = 205
whiteVal = 254

def ensureGrayScale(img):
    try:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        print("\nALERT: CHANGING IMAGE TO GRAYSCALE\n")
    except Exception as e:
        print("\nALERT:")
        print(e)
        print()
    
    return img

def debug_Display(img, rescaleVar = 1):
    if rescaleVar!=1:
        img = rescale(img,  rescaleVar)
    while True:

        cv2.imshow("debug Display", img)

        if cv2.waitKey(20) and 0xFF == ord('q'):
            break

def rescale(img, resizerVar):
    try:
        height, width, g = img.shape
    except: 
        height, width = img.shape
    displayImg = cv2.resize(img, (int(width*resizerVar), int(height*resizerVar)))
    return displayImg


# Takes each val and tuns into (imgMAX-val)
def reverseArray(img):
    uniques = np.unique(img)
    min = uniques[0]
    max = uniques[len(uniques)-1]

    height, width= img.shape

    maxArr = np.full((height, width), max)
    reverseArr = np.subtract(maxArr, img)

    return reverseArr

# Normalize array between 0 and range value.
# Returns a numpy 2D array with astype uint8
def normalizeArray(img, range):

    img = img.astype("float64")
    img *= range/img.max()
    img = img.astype("int32")
    img = img.astype("uint8")

    return img


def writeToYaml(data):
    outYamlData = {}

# geneartePixelOrigin takes heigh, width of image and the yaml Y and X to convert
# it into realtime y, x pixel coords of the image to help find the origin og the image
def generatePixelOrigin(height, width, yYaml, xYaml, resolution = 0.050000):

    
    xOriginPx = int((-1* xYaml)/(resolution))
    yOriginPx = int((yYaml/resolution) + height)-1

    retPXCoords = [yOriginPx, xOriginPx]

    return retPXCoords


# Bring up cv2 display and have user conveniently click/visually choose
# the desired origin point
# Functin takes in image, imagename, and displayscale
# Retuerns tuple of (y, x) pixel coord of image origin
counter = 0
trueX = 0
trueY = 0
def displayPickOrigin(Orig_img, imageName="Image", displayScale=0.2):

    points = np.zeros((40,2), np.int)
    img = rescale(Orig_img, displayScale)

    # Turn image to rgb:
    if len(img.shape)<3:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

    def mousePoints(event, x, y, flags, params):
        global counter
        global trueX
        global trueY

        if event == cv2.EVENT_LBUTTONDOWN:
            points[counter] = x, y
            counter = counter+1
            
            trueX = int(x/displayScale)
            trueY = int(y/displayScale)

            # print("CHOSEN START POINT COORD: ")
            # print("startY: ", trueY)
            # print("startX: ", trueX)

            # cv2.destroyAllWindows()

    displaySig = 0
    while True:
        for x in range(0, counter+1):
            cv2.circle(img, (points[x][0], points[x][1]), 3, (0,255,0), cv2.FILLED)

        cv2.imshow(imageName, img)
        cv2.setMouseCallback(imageName, mousePoints)


        if displaySig > 0:
            print("SAVING....")
            time.sleep(3)
            break
        

        if counter>0:    
            cv2.imshow(imageName, img) 

            print("CHOSEN START POINT COORD: ")
            print("startY: ", trueY)
            print("startX: ", trueX)
            displaySig = 1
        cv2.waitKey(1)


    returnPoints = (trueY, trueX)

    return returnPoints




# generateYamlVals takes in the dimensions of the image and the desired pixel 
# coords of the origin and converts it to yaml coordinates (typically measured in meters)
# and returns it in the form of a list in [x, y, orientation]
def generateYamlVals(height, width, yOrigin, xOrigin, resolution = 0.050000):
    #^assume all inputs are numbers

    print("\nORIGIN Pixel Coords:")
    print("Y: ", yOrigin)
    print("X: ", xOrigin)

    print()

    yYaml = 0
    xYaml = 0

    # Note for math:

    yYaml = (-1*resolution)*(height - yOrigin)
    xYaml = (-1*resolution)*(xOrigin)

    # Eliminate floating point weirdness:
    # -just round to 6 sig-figs
    yYaml = float("{0:.6f}".format(yYaml))
    xYaml = float("{0:.6f}".format(xYaml))

    # retTup = (yYaml, xYaml)

    # NOTE:
    # REMEMBER: Yamls is [x, y, yaw]

    retYam = [xYaml, yYaml, 0]


    return retYam
    

def generateTranslateYamlVals():
    return 0

# NOTE: this method may be more accurate as it literally simulated a single pixel being rotated around the center
# using the same method numpy rotation method that the image rotation uses
# And to combat gray/blurring, it just finds the most "conentrated" or "highest value" pixel after rotation
# Use the Cheat method:
# Create a numpy array (unit 8) of zeroes, set the origin coord as a non-zero number, rotate it the exact way in the angle
# using cv2, and finally search through the result array to find the highest value coord and record
def generateRotatedYamlVals(height, width, yOrigin, xOrigin, angle, resolution = 0.050000, printB=True):
    print("GENERATING ROTATED COORDS USING NUMPY ARRAY SIMULATION")
    print("......")

    #^assume all inputs are numbers

    print()
    print("ORIGIN Pixel Coords:")
    print("Y: ", yOrigin)
    print("X: ", xOrigin)

    center = (width//2, height//2)

    startMarked = np.zeros((height, width))
    startMarked = startMarked.astype("uint8")

    # Mark the Starting origin:
    startMarked[yOrigin][xOrigin] = whiteVal

    # args(<center>, angle, scale)
    # positive angle for counterclockwise and negative for clockwise
    # Here we are applying a (-1) to the angle to make the rotatoin "clockwise"
    rotation_matrix = cv2.getRotationMatrix2D(center, (-1*angle), 1.0)
    startMarkedRotated = cv2.warpAffine(startMarked, rotation_matrix, (width,height), borderValue=blackVal)

    # Check to see if point has moved off of map:
    if len(np.unique(startMarkedRotated))<=1:
        print("ALERT!: POINT HAS MOVED OFF THE MAP! EXITING!")
        exit()

    maxVal = 0
    coordY = 0
    coordX = 0


    # Is brute force and slow:
    # for ySearch in range(len(startMarkedRotated)):
    #     for xSearch in range(len(startMarkedRotated[0])):
    #         if startMarkedRotated[ySearch][xSearch] > maxVal:
    #             coordY = ySearch
    #             coordX = xSearch
    #             maxVal = startMarkedRotated[ySearch][xSearch]

    result = np.where(startMarkedRotated == np.amax(startMarkedRotated))
    listOfCoords = list(zip(result[0], result[1]))



    print("max points: ", listOfCoords )
    finalCoord = listOfCoords[0]
    coordY=finalCoord[0]
    coordX = finalCoord[1]

    
    
    if printB:
        print("Rotated by ", angle ," degrees Origin coords:")
        print("Y: ", coordY)
        print("X: ", coordX)

    # Call upon our built functions to quickly calculate yaml vals
    retYam = generateYamlVals(height, width, coordY, coordX, resolution)
    print("DONE")

    
    return retYam, finalCoord
    # return retYam, (0, 0)


# NOTE: Using this function will get you within +/- 1 pixel of the true origin as we are using math
# Therefore rounding up/down differently
# Angle is clockwise but because the y-axis of the image points down, we can treat the calculations
# as if we are finding a "counterclockwise" rotation
# ALSO: have not used this function in a while so it is not up to date with eliminating
# floating point math do-hickeys
def raw_generateRotatedYamlVals(height, width, yOrigin, xOrigin, angle, resolution = 0.050000):
    #^assume all inputs are numbers
    print("GENERATING ROTATED COORDS USING TRIG MATH ABOUT THE ORIGIN")

    print()
    print()
    print("\nORIGIN Pixel Coords:")
    print("Y: ", yOrigin)
    print("X: ", xOrigin)


    # Get (y, x) coords relative to the center
    # Note: center shoudl be in form (height//2, width//2)
    # Which translates to (y, x)
    center = (height//2, width//2)

    yRelOrigin = yOrigin - center[0]
    xRelOrigin = xOrigin - center[1]

    print("center: ", center)
    print("rel Origin: y x ", yRelOrigin, xRelOrigin)



    # Remember, treat is as a counterclockwise transformation
    # y' = xsin(angle) + ycos(angle)
    # x' = xcos(angle) - ysin(angle)
    angleInRadians = (angle*math.pi)/180

    # NOTE:
    # Double check the math here in the future to make sure the floating point numbers dont screw up the math
    yRotatedOrigin = (xRelOrigin*math.sin(angleInRadians)) + (yRelOrigin*math.cos(angleInRadians))
    xRotatedOrigin= (xRelOrigin*math.cos(angleInRadians)) - (yRelOrigin*math.sin(angleInRadians))

    print("relative rotated origin y, x: ", yRotatedOrigin, xRotatedOrigin)

    # Convert from possible float to int to get solid pixel vals
    xRotatedOrigin = int(xRotatedOrigin + center[1])
    yRotatedOrigin = int(yRotatedOrigin + center[0])

    print("rotated origin y, x: ", yRotatedOrigin, xRotatedOrigin)

    # printCoord = (yRotatedOrigin, xRotatedOrigin)
    print("Rotated by ", angle ," degrees Origin px Coords: ")
    print("Y: ", yRotatedOrigin)
    print("X: ", xRotatedOrigin)

    yYaml = 0
    xYaml = 0
    yYaml = (-1*resolution)*(height - yRotatedOrigin)
    xYaml = (-1*resolution)*(xRotatedOrigin)

    # Eliminate floating point weirdness:
    # -just round to 6 sig-figs
    yYaml = float("{0:.6f}".format(yYaml))
    xYaml = float("{0:.6f}".format(xYaml))

    # retTup = (yYaml, xYaml)

    # NOTE:
    # REMEMBER: Yamls is [x, y, yaw]

    retYam = [xYaml, yYaml, 0]

    print()
    print("DONE")
    return retYam