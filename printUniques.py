import cv2
import numpy as np

import sys

import utils


savefilename = "testSave.png"
filename = "testImage_2"
readfilename = filename + ".png"
# cv2.namedWindow("Concentration")
# cv2.resizeWindow("Concentration", 640, 480)

def empty(a):
    pass

if __name__=='__main__':
    if len(sys.argv) > 1:
        filename = sys.argv[1]

        if filename.endswith(".png"):
            readfilename = filename
            filename = filename[:-(len(".png"))]
        else:
            readfilename = filename + ".png"

    else:
        print("ERROR")
        # exit()

    print("filename: ", filename)
    print("readfilename: ", readfilename)

    height = 0
    width = 0

    try:
        img = cv2.imread(readfilename, cv2.IMREAD_UNCHANGED)
        # print("IMG SHAPE: ", img.shape)
        height=0
        width=0
        try:
            height, width= img.shape
        except:
            height, width, something= img.shape
        print("Image dimension:", height, width)
        print("UNIQUES IN THIS IMAGE: ", np.unique(img))
    except Exception as e:
        print(e)
        print("Error!: Ensure that you are inputting valid filename without png exstension")
